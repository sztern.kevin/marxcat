from marxcat.main import app

def test_add_kitties():
    c = app.test_client()

    ret = c.get("/add/42").get_json()

    assert ret['value'] == '84'

def test_successor_kitty():
    c = app.test_client()

    ret = c.get("successor/42").get_json()

    assert ret['value'] == '43'

def test_predecessor_kitty():
    c = app.test_client()

    ret = c.get("predecessor/42").get_json()

    assert ret['value'] == '41'

def test_substract_kitty():
    c = app.test_client()

    ret = c.get("substract/44")

    assert ret.status_code == 200

    assert ret.get_json()['value'] == '42'

def test_pastis_kitty():
    c = app.test_client()

    ret = c.get("pastis")

    assert ret.status_code == 200

    assert ret.get_json()['value'] == '51'

