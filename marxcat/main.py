from flask import Flask, jsonify

app = Flask('toto')

@app.route("/")
def index():
    return "Hello kitty!"

@app.route("/add/<int:n>")
def add_kitties(n):
    return jsonify({'value': str(n * 2)})

@app.route("/successor/<int:n>")
def successor_kitty(n):
    return jsonify({'value': str(n + 1)})

@app.route("/predecessor/<int:n>")
def predecessor_kitty(n):
    return jsonify({'value': str(n - 1)})

@app.route("/substract/<int:n>")
def substract_kitty(n):
    return jsonify({'value': str(n - 2)})

@app.route("/pastis")
def pastis_kitty():
    return jsonify({'value': '51'})
